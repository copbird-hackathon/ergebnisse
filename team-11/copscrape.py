#!/usr/bin/env python3

import pandas as pd
import snscrape.modules.twitter as sntwitter
import datetime, time
import os
import plotly.express as px


police_handles = ['PolizeiWittlich','LKA_Bayern','Polizei_SDL','PolizeiRV','Polizei_BadN','Polizei_ZPD_NI','Polizei_KO','PolizeiMainz','polizei_nrw_ob','PP_Rheinpfalz','PolizeiTrier',\
'PolizeiSachsen','Polizei_Ffm','Polizei_nrw_ms','Polizei_NRW_E','Polizei_NRW_AC','polizei_nrw_bi','polizei_nrw_bo','polizei_nrw_bn','polizei_nrw_bor',\
'polizei_nrw_coe','polizei_nrw_du','polizei_nrw_dn','polizei_nrw_d','polizei_nrw_w','polizei_nrw_en','polizei_nrw_waf','polizei_nrw_wes','polizei_nrw_un',\
'polizei_nrw_eu','polizei_nrw_vie','polizei_nrw_st','polizei_nrw_ge','polizei_nrw_gt','polizei_nrw_so','polizei_nrw_si','polizei_nrw_su','polizei_nrw_rkn',\
'polizei_nrw_rbk','polizei_nrw_re','polizei_nrw_rek','polizei_nrw_pb','polizei_nrw_gm','polizei_nrw_oe','polizei_nrw_mi','polizei_nrw_me','polizei_nrw_mk',\
'polizei_nrw_mg','polizei_nrw_lip','polizei_nrw_hx','polizei_nrw_kr','polizei_nrw_hsk','polizei_nrw_kle','polizei_nrw_hs','polizei_nrw_ham','polizei_nrw_hf',\
'polizei_nrw_ha','polizeiberlin','PolizeiBerlin_E','Polizei_PS','polizei_nrw_k','PolizeiMuenchen','Polizei_MD','PolizeiHamburg','polizeiOBS','Polizei_KA',\
'Polizei_Thuer','Polizei_NH','bpol_bw','bpol_by','Polizei_PP_NB','LKA_Hessen','PolizeiVG','Polizei_SN','Polizei_HST','PolizeiBhv','Polizei_OH','PolizeiMannheim',\
'PP_Stuttgart','Polizei_MSE','Polizei_soh','Polizei_DeRo','Polizei_FT','Polizei_HAL','PolizeiKonstanz','bpol_b','bpol_koblenz','bpol_kueste','bpol_bepo',\
'bpol_air_fra','bpol_nord','bpol_pir','bpol_nrw','bpol_b_einsatz','Polizei_MH','PolizeiBB','PolizeiUFR','Polizei_WH','PolizeiBayern','polizeiNB','PolizeiLB',\
'Polizei_SuedHE','Polizei_BS','Polizei_OS','PolizeiNI_lka','polizei_nrw_do','Polizei_LG','Polizei_H','Polizei_WL','Polizei_GOE','Polizei_HI','Polizei_EL',\
'Polizei_HM','Polizei_NBG','Polizei_LER_EMD','Polizei_AUR_WTM','Polizei_NOM','Polizei_HOL','Polizei_OHA','Polizei_STH','Pol_Grafschaft','Polizei_STD','Polizei_BBG',\
'Polizei_Rostock','Polizei_OL','PolizeiHN','PolizeiUL','polizeiSWN','Polizei_ROW','Polizei_CE','PolizeiAalen','PolizeiOFR','PolizeiSWS','polizeiopf','PolizeiMFR',\
'BremenPolizei','PolizeiOG','SH_Polizei','Polizei_HK','LkaBaWue','polizeiOBN','PolizeiSaarland','PolizeiRT','Polizei_WOB','PolizeiFR','Polizei_GER','PolizeiNeustadt',\
'bpol_11','Polizei_GF','Polizei_KL','Polizei_GS','LKA_RLP','Polizei_SZ','PolizeiBB_E','Polizei_CUX','Polizei_DH','Polizei_WHV_FRI','Polizei_DEL','Polizei_CLP_VEC',\
'Polizei_VER_OHZ','Polizei_PP_ROS']

limit = None

names = []
usernames = []
followers = []

for row in police_handles:
    twitter_user_in = sntwitter.TwitterUserScraper(row, False)
    names += [ str(twitter_user_in.entity.displayname) ]
    usernames += [ str(twitter_user_in.entity.username) ]
    followers += [ int(twitter_user_in.entity.followersCount) ]

# Existenzprüfung von ./cops.csv
if os.access('./cops.csv', mode = os.O_RDONLY) is False:
    add_headers = True
else:
    add_headers = False

d = {'names' : names, 'user_id': usernames, 'followers': followers, 'date': str(datetime.datetime.now(datetime.timezone.utc).strftime('%Y-%m-%d %H:%M:%S'))}
df = pd.DataFrame(data = d)
#df
df.to_csv('./cops.csv', index = False, mode = 'a+', header = add_headers)

df_plot = pd.read_csv('./cops.csv')
fig = px.line(df_plot, x="date", y="followers", color="names", hover_data=['user_id'], title="Twitter: Entwicklung der Polizei-Account Follower", labels = {'names':'Pozileidirektion','user_id':'Account', 'followers': 'Follower', 'date':'Datum'})
fig.update_traces(mode='markers+lines')
fig.update_layout(xaxis_title = 'Datum', yaxis_title = 'Follower', xaxis_tickformat = '%Y-%m-%d %H:%M:%S', yaxis_tickformat = ',', legend_traceorder="reversed")
fig.write_html("./index.html")

fig.show() # TODO: Zeitangabe nicer machen, evtl. gruppieren? nach Bundesland? nach landeskriminalamt/polizei? oder urban vs. rural oder so?
