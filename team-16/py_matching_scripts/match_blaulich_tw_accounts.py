import pandas as pd
from os import listdir
from os.path import join, isdir

df_tw_user = pd.read_csv('copbird_table_user_ext.csv').rename(columns={'name': 'user_name'})
dir_blaulicht = 'data/presseportal'

def concat_blaulicht_dfs():
    df = pd.DataFrame()
    for dir in listdir(dir_blaulicht):
        dir = join(dir_blaulicht, dir)
        if isdir(dir):
            for f in listdir(dir):
                f = join(dir, f)
                csv = pd.read_csv(f)
                df = df.append(csv)
    return df

def extend_blaulicht_data():
    df_blaulicht = concat_blaulicht_dfs()
    mapping = map_bl_tw_citys()
    df_blaulicht['tw_user_id'] = df_blaulicht['location'].apply(lambda x: find_location(x, mapping))
    return df_blaulicht

def find_location(txt, mp):
    mapped_blaulicht = mp.get(txt, "")
    return mapped_blaulicht[1] if mapped_blaulicht != "" else ""

def map_bl_tw_citys():
    import re
    df_blaulicht = concat_blaulicht_dfs()
    df_blaulicht.sort_index(inplace=True)
    tw_locations = list(df_tw_user[['stadt', 'user_id']].itertuples(index=False, name=None))
    tw_locations = [(loc, id) for loc, id in tw_locations if len(str(loc)) > 1]
    bl_locations = list(set([str(city) for city in df_blaulicht['location'].values]))
    bl_tw_locations = {}
    for bl_loc in bl_locations:
        for tw_loc, tw_id in tw_locations:
            if re.search(r'\b' + re.escape(str(tw_loc).lower()) + r'\b', str(bl_loc).lower()):
                bl_tw_locations[bl_loc] = [tw_loc, tw_id]
    return bl_tw_locations

if __name__ == '__main__':
    extend_blaulicht_data()
