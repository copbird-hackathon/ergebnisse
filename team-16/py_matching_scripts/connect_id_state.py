"""
Aim: Building a connection between all tweets (tweet-id) and the state (Bundesland; Stadt) of the corresponding
police station (user_id; name; handle)
"""

import pandas as pd
from match_blaulich_tw_accounts import extend_blaulicht_data

tw_tweets = pd.read_csv(r'data\copbird_table_tweet.csv')
tw_user_data = pd.read_csv(r'data\copbird_table_user.csv')
tw_pol_geo_data = pd.read_csv(r'geolocations\polizei_accounts_geo.csv', delimiter='\t')


def get_tweets_by_user_id():
    tweet_ids_user_ids = pd.DataFrame(tw_tweets, columns=['user_id', 'id'], dtype=str).rename(
        columns={"id": "tweet_id"})
    grouped_tweets = tweet_ids_user_ids.groupby('user_id')
    return grouped_tweets


def add_state_to_user_df():
    tw_user_df = tw_user_data.rename(columns={"id": "user_id"})
    tw_pol_geo_df = tw_pol_geo_data.rename(columns={"Name": "name", "Bundesland": "bundesland", "Stadt": "stadt"})

    return pd.merge(tw_user_df, tw_pol_geo_df[['name', 'stadt', 'bundesland']], on='name', how='left')


def add_state_to_tweets_df():
    tw_tweets_ext = pd.merge(tw_tweets, add_state_to_user_df()[['user_id', 'stadt', 'bundesland', 'name', 'handle'
                                                                ]], on='user_id', how='left')
    return tw_tweets_ext[['id', 'tweet_text', 'created_at', 'user_id', 'name', 'handle', 'stadt', 'bundesland'
                          ]].rename(columns={'id': 'tweet_id', 'name': 'user_name'})


def save_to_csv(df: pd, file_name: str):
    df.to_csv(path_or_buf=f'{file_name}.csv', index=False)


if __name__ == '__main__':
    save_to_csv(extend_blaulicht_data(), '2020-12_2021-05_presseportal')
