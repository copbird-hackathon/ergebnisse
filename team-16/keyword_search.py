import pandas as pd
import spacy
from string import punctuation
from tqdm import tqdm
tqdm.pandas()


tw_tweets = pd.read_csv(r'data\copbird_table_tweet_ext_state.csv')

nlp = spacy.load('de_core_news_lg')


def clean_tweet(txt):
    doc = nlp(txt)
    token_list = []

    for token in doc:
        if (token.text not in punctuation) and (token.is_stop is False):
            token_list.append(token.lemma_)
        else:
            pass
    return ' '.join(token_list)


def get_topics_by_str_lst(topic, df, col_name):
    df_topiced = df[df[col_name].str.contains('|'.join(topic))]
    return df_topiced


if __name__ == '__main__':
    topic_1 = ['demonstr', 'kundgeb']
    topic_2 = ['drogen', 'weed', 'graas', 'lsd', 'cannabis', 'ecstasy', 'kokain', 'meth', 'crystal']
    topic_3 = ['rassis', 'diskriminier', 'ausländerfeindlich', 'fremdenfeindlich', 'fremdenhass']
    topic_4 = ['antisem', 'juden', 'synagoge', 'judenhass', 'judenfeindlich', 'holocaust']

    df_pm = pd.read_csv(r'data\2020-12_2021-05_presseportal.csv', na_filter=False)
    df_pm_col = 'content'

    print(get_topics_by_str_lst(topic=topic_3, df=df_pm, col_name=df_pm_col).to_markdown())
