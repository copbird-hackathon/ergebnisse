# CopBird Group 16

## Tools

* [Python](https://www.python.org/) Version >= 3.8
* [Wekan](https://wekan.github.io/) als Arbeitsgrundlage mit einem Gruppen-Wekan-Board sowie Gesamt-Boards
* [Matrix](https://matrix.org/) zur Kommunikation

Jupyter Notebook verwendet Kernels. Um ein Environment als Kernel zu verwenden, gibt es folgende
Anleitung: https://queirozf.com/entries/jupyter-kernels-how-to-add-change-remove

## Daten

Die Tweets können nicht öffentlich gemacht werden, jedoch sind die Pressemitteilungen und Sentiment-Wörter unter data/ zu finden.

## Ergebnisse

Die wichtigsten Ergebnisse befinden sich in der [Presse-vs.-Twitter](Presse-vs.-Twitter.pdf) PDF.
In den notebooks finden sich zusätzliche Details zur Datenextraktion und Analyse.

